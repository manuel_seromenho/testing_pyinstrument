## Example for testing Pyinstrument profiler.

## python version
python 2.7

## installing the requirements
$pip install -r requirements.txt

Example of usage of Pyinstrument directly in the code:

 1º  Do the code changes needed:
```
    from pyinstrument import Profiler
    from django_test1_python27 import utils

    profiler = Profiler()

    profiler.start()
    c()
    b()
    a()
    profile_session = profiler.stop()

    output_html = profiler.output_html()
    utils.save_output_html(output_html, profile_session, "/home/username/Desktop/profiles")
```

2º Run the script

```$ python manage.py runserver 0.0.0.0:8000```

3º Access the url

```http://0.0.0.0:8000/example/```

4º By default the output will be saved in the folder "profiles" inside the project, but its possible to indicate the folder as shown in the example of code above.

Note:
As the Pyinstrument does not export the file when profiling directly in the code, I created a small module ```django_test1_python27.utils``` to ensure the output of the html to a file.
