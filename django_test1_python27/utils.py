import io
import os
import time


def save_output_html(output_html, profile_session, profile_dir=None):
    """ saves the html output to a file in a specific folder

    :param output_html: string
    :param profile_session: pyinstrument profile session object
    :param profile_dir: string with the folder to save the output html file

    """

    if not profile_dir:
        base_dir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
        profile_dir = base_dir + '/profiles'

    filename = '{timestamp:.0f} {total_time:.3f}s .html'.format(
        total_time=profile_session.duration,
        timestamp=time.time(),
    )

    file_path = os.path.join(profile_dir, filename)

    if not os.path.exists(profile_dir):
        os.mkdir(profile_dir)

    with io.open(file_path, 'w', encoding='utf-8') as f:
        f.write(output_html)
