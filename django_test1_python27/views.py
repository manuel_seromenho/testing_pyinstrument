from django.shortcuts import render
from pyinstrument import Profiler

from django_test1_python27 import utils

profiler = Profiler()
profiler.start()


def c():
    total = 0
    for i in range(1000000):
        total = total + i*100
    return 1


def b():
    total = 0
    for i in range(1000000):
        total = total + i * 10
    c()
    return 1


def a():
    total = 0
    for i in range(1000000):
        total = total + i*1
    b()
    return 1


def example(request):
    profiler.start()
    c()
    b()
    a()
    profile_session = profiler.stop()

    output_html = profiler.output_html()
    utils.save_output_html(output_html, profile_session)

    return render(request, 'example.html')
